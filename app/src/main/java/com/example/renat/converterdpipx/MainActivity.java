package com.example.renat.converterdpipx;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private double pxTommldpi(double ldpi) {
        return ldpi*0.2117;
    }

    private double pxTommmdpi(double mdpi) {
        return mdpi*0.1588;
    }

    private double pxTommhdpi(double hdpi) {
        return hdpi*0.1058;
    }

    private double pxTommxhdpi(double xhdpi) {
        return xhdpi*0.0794;
    }

    public void onClick(View view) {
        RadioButton mRBldpi = findViewById(R.id.radioldpi);
        RadioButton mRBmdpi = findViewById(R.id.radiomdpi);
        RadioButton mRBhdpi = findViewById(R.id.radiohdpi);
        RadioButton mRBxhdpi = findViewById(R.id.radioxhdpi);
        EditText mETlength = findViewById(R.id.lengthPix);
        EditText mETheight = findViewById(R.id.heightPix);
        TextView mTextView = findViewById(R.id.result);

        if (mETlength.getText().length() == 0) {
            Toast.makeText(getApplicationContext(),R.string.writeSomething, Toast.LENGTH_SHORT).show();
            return;
        }

        if (mETheight.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), R.string.writeSomething, Toast.LENGTH_SHORT).show();
        }

        double lengthValue = Double.parseDouble(mETlength.getText().toString());
        double heightValue = Double.parseDouble(mETheight.getText().toString());

        if (mRBldpi.isChecked()) {
            mTextView.setText(String.valueOf(pxTommldpi(lengthValue)) + getString(R.string.mm) + getString(R.string.xxx) + String.valueOf(pxTommldpi(heightValue)) + getString(R.string.mm));
        }
        if (mRBmdpi.isChecked()) {
            mTextView.setText(String.valueOf(pxTommmdpi(lengthValue)) + getString(R.string.mm) + getString(R.string.xxx) + String.valueOf(pxTommmdpi(heightValue)) + getString(R.string.mm));
        }
        if(mRBhdpi.isChecked()) {
            mTextView.setText(String.valueOf(pxTommhdpi(lengthValue)) + getString(R.string.mm) + getString(R.string.xxx) + String.valueOf(pxTommhdpi(heightValue)) + getString(R.string.mm));
        }
        if(mRBxhdpi.isChecked()) {
            mTextView.setText(String.valueOf(pxTommxhdpi(lengthValue)) + getString(R.string.mm) + getString(R.string.xxx) + String.valueOf(pxTommxhdpi(heightValue)) + getString(R.string.mm));
        }
    }
}
